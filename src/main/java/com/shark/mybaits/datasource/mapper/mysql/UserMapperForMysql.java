/**
 * Copyright (C), 2015-2022, XXX有限公司
 * FileName: UserMapper
 * Author:   lixh
 * Date:     2022/4/16 21:12
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.shark.mybaits.datasource.mapper.mysql;

import com.shark.mybaits.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 〈接口描述〉<br> 
 * 〈〉
 *
 * @author nmgli
 * @create 2022/4/16
 * @since 1.0.0
 */
@Mapper
public interface UserMapperForMysql {
    List<User> getAllUser();

}