/**
 * Copyright (C), 2015-2022, XXX有限公司
 * FileName: UserMapper
 * Author:   lixh
 * Date:     2022/4/16 21:12
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.shark.mybaits.mapper;

import com.shark.mybaits.dto.UserCourseDto;
import com.shark.mybaits.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 〈接口描述〉<br> 
 * 〈〉
 *
 * @author nmgli
 * @create 2022/4/16
 * @since 1.0.0
 */
//@Mapper
public interface UserMapper {

    List<User> getAllUser();

    int updateUserByUserid(User user);

    int deleteUserByUserid(Integer userid);

    int insertUser(User user);

    int insertUserForEach(List<User> list);

    List<UserCourseDto> getAllUserCourseDto();

    List<UserCourseDto> getAllUserCourseResultMap();

    List<Map> getAllUserCourseMap();

    int insertUserBackId(User user);

    User getUserByUsername(@Param("username") String username);
}