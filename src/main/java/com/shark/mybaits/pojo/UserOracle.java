/**
 * Copyright (C), 2015-2022, ttfx
 * FileName: UserDto
 * Author:   lixh
 * Date:     2022/4/23 22:26
 * Description: oracle的用户类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.shark.mybaits.pojo;

/**
 * 〈类描述〉<br> 
 * 〈oracle的用户类〉
 *
 * @author nmgli
 * @create 2022/4/23
 * @since 1.0.0
 */
public class UserOracle {
    private String userid;
    private String username;
    private String password;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "userid='" + userid + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
