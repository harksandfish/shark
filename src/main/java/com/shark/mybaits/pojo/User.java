/**
 * Copyright (C), 2015-2022, ttfx
 * FileName: User
 * Author:   lixh
 * Date:     2022/4/16 21:10
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.shark.mybaits.pojo;

/**
 * 〈类描述〉<br> 
 * 〈〉
 *
 * @author nmgli
 * @create 2022/4/16
 * @since 1.0.0
 */
public class User {
    private Integer userid;
    private String username;
    private String password;
    private Integer phone;
    private Integer sex;
    private String userno;

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno;
    }

    @Override
    public String toString() {
        return "User{" +
                "userid=" + userid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", phone=" + phone +
                ", sex=" + sex +
                ", userno='" + userno + '\'' +
                '}';
    }
}
