/**
 * Copyright (C), 2015-2022, ttfx
 * FileName: UserCourseDto
 * Author:   lixh
 * Date:     2022/4/20 20:49
 * Description: 学生课程的dto类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.shark.mybaits.dto;

/**
 * 〈类描述〉<br> 
 * 〈学生课程的dto类〉
 *
 * @author nmgli
 * @create 2022/4/20
 * @since 1.0.0
 */
public class UserCourseDto{
    private String userno;//用户编码
    private String username;//用户名
    private Integer math;//数学成绩
    private Integer language;//语文成绩
    private Integer sports;//体育成绩

    @Override
    public String toString() {
        return "UserCourseDto{" +
                "username='" + username + '\'' +
                ", userno='" + userno + '\'' +
                ", math=" + math +
                ", language=" + language +
                ", sports=" + sports +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno;
    }

    public Integer getMath() {
        return math;
    }

    public void setMath(Integer math) {
        this.math = math;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public Integer getSports() {
        return sports;
    }

    public void setSports(Integer sports) {
        this.sports = sports;
    }
}
