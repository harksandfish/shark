package com.shark;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@MapperScan("com.shark.mybaits.mapper")
public class SharkApplication {

    public static void main(String[] args) {
        SpringApplication.run(SharkApplication.class, args);
    }

}
