/**
 * Copyright (C), 2015-2022, ttfx
 * FileName: JdbcTest
 * Author:   lixh
 * Date:     2022/4/12 21:33
 * Description: JDBC测试类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.shark.jdbc;

import java.sql.*;

/**
 * 〈类描述〉<br> 
 * 〈JDBC测试类〉
 *
 * @author nmgli
 * @create 2022/4/12
 * @since 1.0.0
 */
public class JdbcTest {

    /**
     * jdbc连接mysql数据库的步骤
     *         1、加载驱动
     *         2、连接数据库DriverManager
     *         3、获得执行sql的对象Statement
     *         4、获得返回的结果集
     *         5、释放连接
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {


            String jdbcName="com.mysql.jdbc.Driver";//定义驱动程序名
            String dbUserName="root";//用户名
            String dbPassword="root";//密码
            //数据库连接的url
            String dbUrl="jdbc:mysql://localhost:3306/sywk?"+"useUnicode=true&characterEncoding=UTF8";
            String sql="select * from sywk_user";//定义查询语句

            Class.forName(jdbcName);//注册驱动程序
            Connection con= DriverManager.getConnection(dbUrl,dbUserName,dbPassword);//获取数据库连接
            Statement stmt=con.createStatement();


            ResultSet rs=stmt.executeQuery(sql);//执行sql并返还结束 ；ResultSet executeQuery(String sqlString)：用于返还一个结果集（ResultSet）对象。

            //遍历结果集
            while(rs.next()){
                System.out.println("用户编号："+rs.getInt("userid")+"，用户名："+rs.getString("username")+"，密码："+rs.getString("password")+"，手机号："+rs.getString("phone"));
            }

            if(rs!=null){//关闭记录集
                try{
                    rs.close();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }

            if(stmt!=null){//关闭说明对象
                try{
                    stmt.close();
                }catch(SQLException e){
                    e.printStackTrace();
                }
            }

            if(con!=null){//关闭连接，就像关门一样，先关里面的，最后关最外面的
                try{
                    con.close();
                }catch(SQLException e){
                    e.printStackTrace();
                }

            }
        }






}
