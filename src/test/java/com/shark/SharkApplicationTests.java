package com.shark;

import com.shark.mybaits.datasource.mapper.mysql.UserMapperForMysql;
import com.shark.mybaits.datasource.mapper.oracle.UserMapperForOracle;
import com.shark.mybaits.dto.UserCourseDto;
import com.shark.mybaits.mapper.UserMapper;
import com.shark.mybaits.pojo.User;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SpringBootTest
class SharkApplicationTests {


    //@Autowired
    private UserMapper userMapper;

    //查询所有数据
    @Test
    public void getAllUser(){
        List<User> all = userMapper.getAllUser();
        for (User user:all){
            System.out.println(user);
        }
    }

    //根据id修改用户信息
    @Test
    public void updateUserByUserid(){
        User user = new User();
        user.setUserid(3);
        user.setUsername("lisi-update");
        user.setPassword("password-update");
        user.setPhone(139);
        int result = userMapper.updateUserByUserid(user);
        if(result > 0){
            System.out.println("用户信息更新成功");
        }else{
            System.out.println("用户信息更新失败");
        }
    }

    //根据id删除用户信息
    @Test
    public void deleteUserByUserid(){

        int result = userMapper.deleteUserByUserid(11);
        if(result > 0){
            System.out.println("用户信息删除成功");
        }else{
            System.out.println("用户信息删除失败");
        }
    }

    @Test
    public void insertUser(){
        User user = new User();
        user.setUsername("user666");
        user.setPassword("666");
        user.setPhone(139);
        int result = userMapper.insertUser(user);
        if(result > 0){
            System.out.println("用户新增成功");
        }else{
            System.out.println("用户新增失败");
        }
    }



    //保存多个怎么办？
    @Test
    public void insertUserFor(){
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setUsername("user" + i);
            user.setPassword("password" + i);
            user.setPhone(139);
            int result = userMapper.insertUser(user);
            if(result > 0){
                System.out.println("用户新增成功" + i);
            }else{
                System.out.println("用户新增失败" + i);
            }
        }
    }

    //上一种方式、效率怎么样？
    @Test
    public void insertUserForeach(){

        List<User> list = new ArrayList<>();
        for (int i = 1; i < 6; i++) {
            User user = new User();
            user.setUsername("nuser" + i);
            user.setPassword("npassword" + i);
            user.setPhone(139);
            list.add(user);
        }

        int result = userMapper.insertUserForEach(list);
        if(result > 0){
            System.out.println("用户新增成功");
        }else{
            System.out.println("用户新增失败");
        }

    }

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Test
    public void batchSave(){
        List<User> userList = new ArrayList<>();
        for (int i = 1; i < 6; i++) {
            User user = new User();
            user.setUsername("cuser" + i);
            user.setPassword("cpassword" + i);
            user.setPhone(139);
            userList.add(user);
        }
        //使用批处理
        long start = System.currentTimeMillis();
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        userList.stream().forEach(user -> userMapper.insertUser(user));
        sqlSession.commit();
        sqlSession.clearCache();
        System.out.println(System.currentTimeMillis() - start);
    }



    //查询所有数据
    @Test
    public void getAllUserCourseDto(){
        List<UserCourseDto> list = userMapper.getAllUserCourseDto();
        for (UserCourseDto userCourseDto:list){
            System.out.println(userCourseDto);
        }
    }


    @Test
    public void getAllUserCourseResultMap(){
        List<UserCourseDto> list = userMapper.getAllUserCourseResultMap();
        for (UserCourseDto userCourseDto:list){
            System.out.println(userCourseDto);
        }
    }
    //查询所有数据
    @Test
    public void getAllUserCurseMap(){
        List<Map> list = userMapper.getAllUserCourseMap();
        for (Map map:list){
            System.out.println(map.toString());
        }
    }



    @Test
    public void insertUserBackId(){
        User user = new User();
        user.setUsername("user88");
        user.setPassword("88");
        user.setPhone(139);
        int result = userMapper.insertUserBackId(user);
        if(result > 0){
            System.out.println("用户新增成功" + user.getUserid());
        }else{
            System.out.println("用户新增失败");
        }
    }

    //使用用户名查询
    @Test
    public void getUserByUsername(){
        String username="lisi";
        User user = userMapper.getUserByUsername(username);
        System.out.println("getUserByUsername"+user.toString());
    }

    @Autowired
    UserMapperForMysql userMapperForMysql;

    @Autowired
    UserMapperForOracle userMapperForOracle;

    @Test
    void getAllUserForMoreDataSource(){
        System.out.println("userMapperForMysql.getAllUsers() = " + userMapperForMysql.getAllUser());
        System.out.println("userMapperForOracle.getAllUsers() = " + userMapperForOracle.getAllUser());
    }




}
